import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Button, Card } from 'react-native-paper';
import { DefaultTheme } from 'react-native-paper';
import AppButton from '../components/AppButton';

function ProductsScreen ({ navigation }) {
  return (
    <ScrollView style={styles.scrollView}>
      <Card style={styles.mainCard}>
        <Card style={styles.card}>
          <Card.Title title="Available Items to Track" />
          <Card.Content>
          </Card.Content>
        </Card>

        
      </Card>
      <Card style={styles.lastCard}>
          <Card.Content>
            <AppButton title={"add new"} icon={"plussquare"} onPress={() => navigation.navigate('Add Product')} />
          </Card.Content>
        </Card>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: DefaultTheme.colors.background,
    paddingTop: 5
  },
  card: {
    width: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingBottom: 50,
    marginBottom: 30,
    marginTop: 20,
    backgroundColor: DefaultTheme.colors.primary,
  },
  mainCard: {
    // borderTopColor: 'aqua',
    borderTopEndRadius: 50,
    backgroundColor: DefaultTheme.colors.backdrop,
    // borderTopWidth: 3,
    width: '95%',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  lastCard: {
    width: '50%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 30,
    marginBottom: 20
  }
});

export default ProductsScreen