import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { Card } from 'react-native-paper';
import { DefaultTheme } from 'react-native-paper';
import Icon from 'react-native-vector-icons/AntDesign';
import AppButton from '../components/AppButton';

function HomeScreen ({ navigation }) {
  return (
    <ScrollView style={styles.scrollView}>
      <Card style={styles.card}>
        <Card.Title title="My Tracked Items" />
        <Card.Content>
          <AppButton title={"Products"} icon={"database"} onPress={() => navigation.navigate('Products')} />
        </Card.Content>
      </Card>

      <Card style={styles.card}>
        <Card.Title title="Add a New Product" />
        <Card.Content>
          <AppButton title={"add"} icon={"plussquare"} onPress={() => navigation.navigate('Add Product')} />
        </Card.Content>
      </Card>

      <Card style={styles.card}>
        <Card.Title title="Test Button" />
        <Card.Content>
          <AppButton title={"Hello"} icon={"pluscircle"} />
        </Card.Content>
      </Card>
      
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: DefaultTheme.colors.background,
    paddingTop: 10
  },
  card: {
    width: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 10,
    backgroundColor: DefaultTheme.colors.backdrop,
  }
});

export default HomeScreen