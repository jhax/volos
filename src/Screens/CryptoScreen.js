import { View } from 'react-native';
import { Provider } from 'react-redux';

import Store from '../Utils/Store';
import { Header, CryptoContainer } from '../components';
import React from 'react';


function CryptoScreen () {
    return (
        <Provider store={Store}>
            <View>
                <Header />
                <CryptoContainer />
            </View>
        </Provider>
    )
}


export default CryptoScreen