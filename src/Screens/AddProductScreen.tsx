import React from "react";
import { View } from "react-native";
import AddProductForm from "../components/AddProductForm";

function AddProductScreen() {
  return (
    <View>
      <AddProductForm/>
    </View>
  );
}

export default AddProductScreen