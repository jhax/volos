import React from "react";
import { Text, View, StyleSheet } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Button, TextInput } from "react-native-paper";
import AppButton from "./AppButton";
import { globalStyles } from "./Styles";
import { Formik } from "formik"; 
import AddNewProduct from "../Actions/AddNewProduct";

function formWorks(values) {
  const response = AddNewProduct()
  console.log(values)
  alert("We got a live one, folks!\n")
}
  

function AddProductScreen() {
  return (
    <View style={globalStyles.Container}>
      <Text style={globalStyles.titleText}>Input Form</Text>
        <Formik 
          initialValues={{
            name: '',
            description: '',
            image: ''
            }}
          onSubmit={(values) => {
            console.log(values);
            console.log("Adding Product Now")
            const ret = AddNewProduct(values);
            console.log("New Vlues:")
            console.log(ret)


          }}
          >
          {(formikProps) => (
            <View style={globalStyles.formContainer}>
              <TextInput 
                style={globalStyles.input}
                placeholder='Product Name'
                onChangeText={formikProps.handleChange('name')}
                value={formikProps.values.name}
              />

              <TextInput
                multiline
                style={globalStyles.input}
                placeholder='Product Description'
                onChangeText={formikProps.handleChange('description')}
                value={formikProps.values.description}
              />
              
              <TextInput
                style={globalStyles.input}
                placeholder='Image URL (full URL to product buy page)'
                onChangeText={formikProps.handleChange('image')}
                value={formikProps.values.image}
              />
              <AppButton title={"Submit"} onPress={formikProps.handleSubmit}/>
            </View>
          )}
        </Formik>
      </View>
  );
};



export default AddProductScreen