import * as React from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';

function Hero () {
    return (
      <View style={styles.container}>
       <Image style={styles.logo} source={require('../../assets/img/hero.jpg')} />
        <Text style={styles.paragraph}>
          Form in React Native, The right Way! 
        </Text>
      </View>
    );
  }

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    flex:1,
  },
  paragraph: {
    margin: 24,
    marginTop: 0,
    fontSize: 34,
    fontWeight: 'bold',
    textAlign: 'center',
    color:'#FFF'
  },
  logo: {
   width:'100%',
   height:200
  }
});

export default Hero