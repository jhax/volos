import { StyleSheet } from "react-native";

export const globalStyles = StyleSheet.create({
    titleText: {
        // fontFamily: 'nunity-bold',
        fontSize: 18,
        color: '#333',
    },
    paragraph: {
        marginVertical: 8,
        lineHeight: 20,
    },
    input: {
        borderWidth: 1,
        borderColor: 'blue',
        borderRadius: 5,
        height: 50,
        width: 300,
        backgroundColor: 'white'
      },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        padding: 24,
        backgroundColor: '#181e34',
      },
    formContainer: {
        justifyContent: "center",
        alignContent: "center",
        padding: 8,
        marginLeft: "auto",
        marginRight: "auto",
      },
      button: {
        backgroundColor: 'red',
      },
})