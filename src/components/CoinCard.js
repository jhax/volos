import React from 'react';
import { 
    View,
    Text,
    StyleSheet,
    Image,
    ImageBackground
} from 'react-native';
import { images } from '../Utils/CoinIcons';

const styles = StyleSheet.create({
    container: {
        display: "flex",
        marginBottom: 20,
        borderBottomColor: "#e5e5e5",
        borderBottomWidth: 3,
        padding: 20
    },
    upperRow: {
        display: "flex",
        flexDirection: "row",
        marginBottom: 15
    },
    coinSymbol: {
        marginTop: 10,
        marginLeft: 20,
        marginRight: 5,
        fontWeight: "bold",        
    },
    coinName: {
        marginTop: 10,
        marginLeft: 5,
        marginRight: 20
    },
    seperator: {
        marginTop: 10,
    },
    coinPrice: {
        marginTop: 10,
        marginLeft: "auto",
        marginRight: 10,
        fontWeight: "bold",        
    },
    image: {
        width: 35,
        height: 35,
    },
    bgImage: {
        flex: 1,
        justifyContent: "center"
    },
    moneySymbol: {
        fontWeight: "bold",
    },
    statisticsContainer: {
        display: "flex",
        borderTopColor: "#FAFAFA",
        borderTopWidth: 2,
        padding: 10,
        flexDirection: "row",
        justifyContent: "space-around"
    },
    percentChangePlus: {
        color: "#00BFA5",
        fontWeight: "bold",
        marginLeft: 5
    },
    percentChangeMinus: {
        color: "#DD2C00",
        fontWeight: "bold",
        marginLeft: 5
    }
})

const { 
    container,
    image,
    moneySymbol,
    upperRow,
    coinSymbol,
    coinName,
    coinPrice,
    statisticsContainer,
    seperator,
    percentChangePlus,
    percentChangeMinus
} = styles;

const CoinCard = ({ symbol, coin_name, price_usd, price_thb, percent_change_24h, percent_change_7d, product_image }) => {

    return (
        <View style={container}>
            <View style={upperRow}>
                <Image style={styles.image} source={{ uri: product_image }}/>
                <Text style={coinSymbol}>{symbol}</Text> 
                <Text style={seperator}>|</Text>
                <Text style={coinName}>{coin_name}</Text>
                <Text style={coinPrice}>{price_usd}
                    <Text style={moneySymbol}> $ </Text>
                </Text>
            </View>

            <View style={upperRow}>
                <Text style={coinPrice}>{price_thb}
                    <Text style={moneySymbol}> $ </Text>
                </Text>                    
            </View>

            <View style={statisticsContainer}>
                <Text>Wanted $ 
                     <Text style={coinPrice}>{price_thb}</Text>
                </Text>
                <Text>Current $ 
                    <Text style={percentChangeMinus}>{price_usd}</Text>
                </Text>
            </View>
        </View> 
    );
}



export default CoinCard;