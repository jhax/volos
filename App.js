import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import HomeScreen from './src/Screens/HomeScreen';
import ProductsScreen from './src/Screens/ProductsScreen';
import AddProductScreen from './src/Screens/AddProductScreen';

const Stack = createNativeStackNavigator()

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#3498db',
    accent: '#f1c40f',
    background: '#000',
    backdrop: '#FFFFFF',
  },
};


export default function App() {
  return (
    <PaperProvider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="Products" component={ProductsScreen} />
          <Stack.Screen name="Add Product" component={AddProductScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </PaperProvider>
  );
}
